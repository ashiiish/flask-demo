from flask import Flask, render_template, send_from_directory, request

app = Flask(__name__)


@app.route('/')
def main_page():
    return send_from_directory('static', filename='main.html')


@app.route('/static/<filename:filename>')
def static_content(filename):
    return send_from_directory('static', filename=filename)


@app.route("/find_data", methods=['POST'])
def find_data():
    return render_template(
        'welcome_user.html',
        username=do_something_for_user(request.form['username'])
    )


def do_something_for_user(username)
    return "Hello user: {}".format(username)


if __name__ == '__main__':
    app.run()